INSTRUCCIONES PARA CONECTAR EL CLIENTE DE STREAMING

Estos son los parámetros que hay que modificar en el archivo index.html.

Línea 121: sustituir el valor de source src, y el tipo de streaming, si es necesario.

Línea 129: logo del sitio.

Línea 134: URL del sitio, en sustitución de https://altomarketing.com

Línea 140:  Cambiar lo respectivo en '?text=Mi Radio, el sitio de radios argentinas&url=https://altomarketing.com&original_referer=https%3A%2F%2Ftwitter.com%2Fshare%3Ftext%3DMi Radio, el sitio de radios argentinas%29%26counturl%3Dhttps://altomarketing.com" '.


Insertar los feeds de redes: 

Instrucciones para el feed de twitter:
Leer aquí: https://help.twitter.com/es/using-twitter/embed-twitter-feed, para mejor comprensión. 
Ubicar <div class="twitterfd">. En el código que le sigue, cambiar "OswaldoGomezS" por el nombre de usuario respectivo.
Twitter feed de oswaldogomezs (usuario de ejemplo):
<a class="twitter-timeline" href="https://twitter.com/OswaldoGomezS?ref_src=twsrc%5Etfw">Tweets by OswaldoGomezS</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
---
Instrucciones para el feed de Facebook:
LA APP TIENE QUE ESTAR EN PRODUCCIÓN. (Se configura en developers.facebook.com). Hacer login en developers.google.com. Crear una app y darle permisos de lectura. Luego, ir a https://developers.facebook.com/docs/plugins/page-plugin y seleccionar 'show-stream'. Copiar el código y reemplazar el que está ubicado dentro de <div class="fb-page"> ... </div>.

Feed de soyTecnohoy (de ejemplo):
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v6.0&appId=344562376380535&autoLogAppEvents=1"></script>
Código de muestra
<div class="fb-page" data-href="https://www.facebook.com/soyTecnohoy/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/soyTecnohoy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/soyTecnohoy/">Soytecnohoy</a></blockquote></div>
----

Instrucciones para el feed de Instagram.

Cambiar el usuario OswaldoGomezS en el código de ejemplo.


<script src="dist/InstagramFeed.min.js"></script>
<script>
    (function(){
        new InstagramFeed({
            'username': 'OswaldoGomezS',
            'container': document.getElementById("instagram-feed1"),
            'display_profile': true,
            'display_biography': true,
            'display_gallery': true,
            'callback': null,
            'styling': true,
            'items': 8,
            'items_per_row': 2,
            'margin': 1 
        });
    })();
</script>
